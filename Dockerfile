FROM debian:stable-slim
SHELL ["/bin/bash", "-c"]
RUN apt update
RUN apt install git -y
RUN apt install python3 -y
RUN apt install virtualenv -y
RUN git clone https://github.com/aws/aws-elastic-beanstalk-cli-setup.git
RUN python3 ./aws-elastic-beanstalk-cli-setup/scripts/ebcli_installer.py
RUN echo 'export PATH="/root/.ebcli-virtual-env/executables:$PATH"' >> ~/.bash_profile
RUN apt install python -y